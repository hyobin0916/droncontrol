
#1002. Use MSA Style

Date: 2022-04-26

## Status

superceded by [1003. Use Monolithic Style](1003-use-monolithic-sytle.md)

## Context

고려한 아키텍처 스타일로 레이어드,마이크로 커널, MSA를 검토했다.
이중에서 MSA가 가장 펜시해보였다.

## Decision 

그래서 MSA로 결정했다.

## Consequences

각 서버 시스템간 통신은 이벤트로 하고, Event Broker를 뭘로 할지 결정한다.

